FROM python:3.11

RUN pip install -U pip setuptools wheel
RUN pip install pdm

COPY pyproject.toml pdm.lock README.md /app/
COPY code/ data/ docs/ notebooks/ pipelines/ tests/ /app/

WORKDIR /app

RUN pdm install --frozen-lockfile --no-editable

# # docker build -t cicd-project .
