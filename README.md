# mlops3-course-project


## The Data Science Lifecycle Process

The methodology from this repository is used – [dslp](https://github.com/dslp/dslp).

The Data Science Lifecycle Process is a set of prescriptive steps and best practices to enable data science teams to consistently deliver value. It includes issue templates for common data science work types, a branching strategy that fits the data science development flow, and prescriptive guidance on how to piece together all the various tools and workflows required to make data science work.

## Key Topics

- [Branching Strategy](https://github.com/dslp/dslp/blob/main/branching/branch-types.md)
- [Issue Templates](https://github.com/dslp/dslp/blob/main/issue-types/0-overview-issue-types.md)
- [Workflows](https://github.com/dslp/dslp/blob/main/steps.md)
- [Semantic Version of Data Science Assets](https://github.com/dslp/dslp/blob/main/semantic-versioning.md)
- [Labels](.github/labels.yaml)

## Directory Structure

```
├── .github
│   ├── ISSUE_TEMPLATE
│   │   ├── Ask.md
│   │   ├── Data.Aquisition.md
│   │   ├── Data.Create.md
│   │   ├── Experiment.md
│   │   ├── Explore.md
│   │   └── Model.md
│   ├── labels.yaml
│   └── workflows
├── .gitignore
├── README.md
├── code
│   ├── datasets        # code for creating or getting datasets
│   ├── deployment      # code for deploying models
│   ├── features        # code for creating features
│   └── models          # code for building and training models
├── data                # directory is for consistent data placement. contents are gitignored by default.
│   ├── README.md
│   ├── interim         # storing intermediate results (mostly for debugging)
│   ├── processed       # storing transformed data used for reporting, modeling, etc
│   └── raw             # storing raw data to use as inputs to rest of pipeline
├── docs
│   ├── code            # documenting everything in the code directory (could be sphinx project for example)
│   ├── data            # documenting datasets, data profiles, behaviors, column definitions, etc
│   ├── media           # storing images, videos, etc, needed for docs.
│   ├── references      # for collecting and documenting external resources relevant to the project
│   └── solution_architecture.md    # describe and diagram solution design and architecture
├── notebooks
├── pipelines           # for pipeline
└── tests               # for testing code, data, and outputs
    ├── data_validation
    └── unit
```

## More details:

### For code/models:

If you only have one model, keep things flat to keep things simple.

```
code/models
├── __init__.py
├── preprocess.py
├── train.py
└── evaluate.py
```


If you have multiple models deployed, each one needs a directory:

```
code/models
├── __init__.py
├── item_classifier
│   ├── preprocess.py
│   ├── train.py
│   └── evaluate.py
└── thing_forecaster
    ├── preprocess.py
    ├── train.py
    └── evaluate.py
```

### For code/datasets

Directory for each dataset should contain all the code for creating or ingesting that dataset. It can also include the schema and datasheet.

```
code/datasets
├── __init__.py
├── customer_churn
│   ├── customer_churn.py
│   ├── datasheet.yml
│   └── schema.yml
└── customer_purchase_history
    ├── customer_purchase_history.py
    ├── datasheet.yml
    └── schema.yml
```

## Building and Running Docker Image

### Prerequisites

Before you begin, ensure you have Docker installed on your system. For installation instructions, refer to the [official Docker documentation](https://docs.docker.com/get-docker/).

### Building the Docker Image

To build a Docker image for the project, follow these steps:

1. Navigate to the root directory of the project where the `Dockerfile` is located.

2. Run the following command in your terminal:

    ```bash
    docker build -t your_image_name:tag .
    ```

    - `your_image_name` is the name you want to give to your Docker image.
    - `tag` is the tag you want to assign to your image (e.g., `latest`, `v1.0`). If not specified, `latest` is used by default.
    - The period `.` at the end of the command indicates the current directory, which should contain the Dockerfile.

### Running the Docker Container

After successfully building the image, you can run it as a container:

1. To run the container in detached mode (in the background), use the following command:

    ```bash
    docker run -d -p host_port:container_port your_image_name:tag
    ```

    - Replace `host_port` with the port number on your host machine you wish to use.
    - Replace `container_port` with the port number your application is set to run inside the container.
    - Ensure `your_image_name:tag` matches the name and tag used when building your image.

### Verifying the Container is Running

To verify that your container is running, use the command:

```bash
docker ps
```

This command lists all running containers. Look for your container name or ID in the list.

### Accessing the Application

With the container running, access the application by navigating to:

```
http://localhost:host_port
```

Replace `host_port` with the port number you specified when running the container.

### Stopping the Container

When you're done, you can stop the container by running:

```bash
docker stop container_id
```

- Replace `container_id` with the actual ID of your container, which you can find by using the `docker ps` command.


### Generating Reports from Jupyter Notebooks

To create polished, dynamic reports from your Jupyter notebooks, you can utilize `Quarto`, a powerful tool for reproducible data science. Follow the steps below to render your notebooks into a formatted report:

1. **Setup Quarto**: Ensure that Quarto is installed in your environment. Run `pdm sync` if not.

2. **Prepare Your Notebook**: Your Jupyter notebook should contain all the necessary analysis and markup for your report. Make sure to save and close any open sessions to ensure all changes are included.

3. **Render the Report**:
   - Navigate to the project root directory.
   - Open a command line interface and run the following command:
     ```bash
     quarto render docs
     ```
   This command assumes your notebook is stored in a directory named `docs`. Adjust the path according to your directory structure.

4. **View the Report**: After the rendering process is complete, Quarto generates the report in various formats such as HTML, PDF, or Word, depending on your configuration. You can find these files in the output directory specified in your Quarto configuration.
Main report is [here](docs/_site/index.html).

1. **Customization**: Customize the output by editing the Quarto configuration settings or by adding specific options in the command line. For more details on customization, refer to the [Quarto documentation](https://quarto.org/docs).
